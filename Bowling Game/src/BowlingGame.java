public class BowlingGame {

	private int score = 0;

	public BowlingGame() {
		super();
	}

	protected int getScore() {
		return score;
	}

	protected void roll(int pins) {
		
		score += pins;
		
	}

}