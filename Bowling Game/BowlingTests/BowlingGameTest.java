import static org.junit.Assert.*;

import org.junit.Test;


public class BowlingGameTest extends BowlingGame {
	
	BowlingGame game = new BowlingGame();

	@Test
	public void rollingOneScoresOne() {
		game.roll(1);
		assertEquals(1, game.getScore());
		
	}

	@Test
	public void rollingTwoScoresTwo() throws Exception{
		game.roll(2);
		assertEquals(2, game.getScore());
		
	}
	
	
	

}
